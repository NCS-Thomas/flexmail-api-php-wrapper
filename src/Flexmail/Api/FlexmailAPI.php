<?php

require_once 'config.php';

class FlexmailAPI
{
    /** @var SoapClient */
    private $soapClient = null;

    /** @var array */
    private $parameterPathsToEncodeAsArray = [];

    /**
     * Get the request Service Instance
     *
     * @param String $service Requested service name
     *
     * @return Object An instance of the requested service
     */
    public static function service($service)
    {
        $classname = "FlexmailAPI_{$service}";

        return new $classname();
    }

    /**
     * Reove header/error codes from the response
     *
     * @param stdClass $response The response from the API
     *
     * @return stdClass The same stdClass without the header information
     */
    private function stripHeader($response)
    {
        $valuesToStrip = ['header', 'errorCode', 'errorMessage'];

        foreach ($valuesToStrip as $value):
            if (property_exists($response, $value)):
                unset ($response->$value);
            endif;
        endforeach;

        return $response;
    }

    /**
     * @param array $paths
     */
    protected function registerPathsToEncodeAsArray(array $paths): void
    {
        $this->parameterPathsToEncodeAsArray = array_merge($this->parameterPathsToEncodeAsArray, $paths);
    }

    /**
     * Convert two-(or-more)-dimensional arrays to an stdClass object
     *
     * @param array $parameters The array to convert
     * @param string $path
     *
     * @param object $parent The object to convert it to
     *
     * @return object
     */
    protected function parametersToArguments(array $parameters, $path = '', object $parent = null)
    {
        $current = $parent ?: new class
        {
        };

        array_walk(
            $parameters,
            function($value, $index) use ($current, $path) {
                $path = $path.'/'.$index;

                if (!is_array($value)) {
                    $current->{$index} = $value;

                    return;
                }

                if (in_array($path, $this->parameterPathsToEncodeAsArray)) {
                    array_walk(
                        $value,
                        function(&$value) use ($path) {
                            if (is_array($value)) {
                                $value = $this->parametersToArguments($value, $path);
                            }
                        }
                    );

                    $current->{$index} = $value;

                    return;
                }

                $current->{$index} = $this->parametersToArguments($value, $path);
            }
        );

        return $current;
    }

    /**
     * Execute the requested call
     *
     * @param string $service The name of the service to execute
     * @param object|array $parameters All parameter in an assiociative array
     *
     * @return object
     * @throws Exception
     */
    protected function execute($service, $parameters = [])
    {
        if (is_null($this->soapClient)):
            $this->createSoapClient();
        endif;

        $arguments = is_array($parameters) ? $this->parametersToArguments($parameters) : $parameters;
        $arguments->header = $this->getRequestHeader();

        $response = $this->soapClient->__soapCall($service, [$arguments]);

        if ($response->header->errorCode !== 0 || $response->header->errorCode === ""):
            throw new Exception($response->errorMessage, $response->errorCode);
        endif;

        if (!DEBUG_MODE) {
            $response = $this->stripHeader($response);
        }

        return $response;

    }

    /**
     * Create a new SOAP Client
     *
     * @returns void
     */
    private function createSoapClient()
    {
        // create a new SoapClient instance
        $this->soapClient = new SoapClient(
            FLEXMAIL_WSDL,
            array(
                "location" => FLEXMAIL_SERVICE,
                "uri" => FLEXMAIL_SERVICE,
                "trace" => 1,
            )
        );
    }

    /**
     * Function to create the user's personal request header
     *
     * @return stdClass The user's personal header
     */
    private function getRequestHeader()
    {
        //check of module aanwezig is, geef waarschuwing indien niet.
        $header = new stdClass();

        $header->userId = FLEXMAIL_USER_ID;
        $header->userToken = FLEXMAIL_USER_TOKEN;

        return $header;
    }

}