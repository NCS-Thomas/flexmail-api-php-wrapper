<?php

class FlexmailAPI_Message extends FlexmailAPI
{    
    /**
     * Create a new Message
     * 
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "messageType" => array (                     // array mandatory
     *          "messageName"       => "My message",     // string mandatory
     *          "messageText"       => "<html ... />",   // string mandatory
     *          "messageTextMessage => "textual message" // string optional
     *      )
     * );
     * 
     * @param array $parameters Associative array with properties of a
     *                          messageType object
     * 
     * @return object
     * @throws Exception
     */
    public function create ($parameters)
    {       
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("CreateMessage", $request);

        return $response;
    }

    /**
     * Update an Message
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "messageType" => array (                     // array mandatory
     *          "messageId"         => 1254355           // int mandatory
     *          "messageName"       => "My message",     // string optional
     *          "messageText"       => "<html ... />",   // string optional
     *          "messageTextMessage => "textual message" // string optional
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of a
     *                          messageType object
     *
     * @return object
     * @throws Exception
     */
    public function update ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("UpdateMessage", $request);
        return $response;
    }

    /**
     * Delete a Message
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      "messageType" => array (                     // array mandatory
     *          "messageId"         => 1254355           // int mandatory
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of a
     *                          messageType object
     *
     * @return object
     * @throws Exception
     */
    public function delete ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("DeleteMessage", $request);
        return $response;
    }

    /**
     * Get all Messages
     *
     * Parameters example:
     * ------------------
     * $parameters = [
     *      'messageArchived' => 1,
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getAll($parameters = [])
    {
        return $this->execute('GetMessages', $parameters);
    }
}