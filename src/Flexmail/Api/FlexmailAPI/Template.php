<?php

class FlexmailAPI_Template extends FlexmailAPI
{    
    /**
     * Create a new Template
     * 
     * Parmeters example:
     * ------------------
     * $parameters = array (
     *      "templateType" => array (            // array mandatory
     *          "templateName" => "My template", // string mandatory
     *          "templateText" => "<html ...>",  // string mandatory
     *      )
     * );
     * 
     * @param array $parameters Associative array with properties of a
     *                          templateType object
     * 
     * @return templateId
     */
    public function create ($parameters)
    {   
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("CreateTemplate", $request);
        return $response;
                
    }
     
    /**
     * Update a Template
     * 
     * Parmeters example:
     * ------------------
     * $parameters = array (
     *      "templateType" => array (            // array mandatory
     *          "templateId"   => 124984         // int mandatory
     *          "templateName" => "My template", // string optional
     *          "templateText" => "<html ...>",  // string optional
     *      )
     * );
     * 
     * @param array $parameters Associative array with properties of an
     *                          templateType object
     * 
     * @return void
     */
    public function update ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("UpdateTemplate", $request);
                      
        return $response;
        
    }

    /**
     * Delete a Template
     * 
     * Parmeters example:
     * ------------------
     * $parameters = array (
     *      "templateType" => array (  // array mandatory
     *          "templateId" => 124984 // int mandatory
     *      )
     * );
     * 
     * @param array $parameters Associative array with properties of a
     *                          templateType object
     * 
     * @return void
     */
    public function delete ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("DeleteTemplate", $request);
        return $response;
        
    }

    /**
     * Get all Templates
     *
     * @return object
     * @throws Exception
     */
    public function getAll()
    {
        return $this->execute('GetTemplates');
    }
    
}