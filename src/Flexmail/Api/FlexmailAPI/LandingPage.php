<?php
class FlexmailAPI_LandingPage extends FlexmailAPI
{
    /**
     * Create a new Landing Page
     *
     * @param array $parameters Associative array with properties of a landingPageType object
     *
     *  Parmeters example:
     * ------------------
     * $parameters = array (
     *      "landingPageType"     => array (                  // array mandatory
     *          "landingPageName" => "My landing page",       // string mandatory
     *          "landingPageText" => "HTML code landing page" // string mandatory
     *      )
     * );
     *
     * @return landingPageId
     */
    public function create ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute("CreateLandingPage", $request);
        return $response;

    }

    /**
     * Update a Landing Page
     *
     *  Parmeters example:
     * ------------------
     * $parameters = array (
     *      "landingPageType"     => array (            // array mandatory
     *          "landingPageId    => 14954
     *          "landingPageName" => "My Landing Page", // string optional
     *          "landingPageText" => "HTML code"        // string optional
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of an landingPageType object
     *
     * @return void
     */
    public function update ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute("UpdateLandingPage", $request);
        return $response;
    }

    /**
     * Delete a Landing Page
     *
     *  Parmeters example:
     * ------------------
     * $parameters = array (
     *      "landingPageType"     =>  array ( // array mandatory
     *          "landingPageId"   =>  14954   // intmandatory
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of an landingPageType object
     *
     * @return void
     */
    public function delete ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute("DeleteLandingPage", $request);
        return $response;
    }

    /**
     * Get all Landing Pages
     *
     * @return object
     * @throws Exception
     */
    public function getAll()
    {
        return $this->execute('GetLandingPages');
    }
}