<?php

class FlexmailAPI_Campaign extends FlexmailAPI
{
    /**
     * Create a new Campaign
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignType' => [
     *          'campaignName' => 'Test Campaigns',
     *          'campaignSubject' => 'Test subject',
     *          'campaignSenderName' => 'Flexmail',
     *          'campaignMessageId' => 207375,
     *          'campaignMailingIds' => [116911],
     *          'campaignReplyEmailAddress' => 'bar@flexmail.eu',
     *          'campaignSenderEmailAddress' => 'foo@flexmail.eu',
     *      ]
     *  ];
     *
     * @param  array $parameters
     * @return object
     * @throws Exception
     */
    public function create($parameters)
    {
        $this->registerPathsToEncodeAsArray([
            '/campaignType/campaignMailingIds',
        ]);

        return $this->execute('CreateCampaign', $parameters);
    }

    /**
     * Update a Campaign
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignType' => [
     *          'campaignId' => 123456,
     *          'campaignName' => 'Test Campaigns update',
     *          'campaignSubject' => 'Test subject',
     *          'campaignSenderName' => 'Flexmail',
     *          'campaignMessageId' => 207375,
     *          'campaignMailingIds' => [116911],
     *          'campaignReplyEmailAddress' => 'bar@flexmail.eu',
     *          'campaignSenderEmailAddress' => 'foo@flexmail.eu',
     *      ]
     *  ];
     *
     * @param  array $parameters
     * @return object
     * @throws Exception
     */
    public function update($parameters)
    {
        $this->registerPathsToEncodeAsArray([
            '/campaignType/campaignMailingIds',
        ]);

        return $this->execute('UpdateCampaign', $parameters);
    }

    /**
     * Delete a Campaign
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignType' => [
     *          'campaignId' => 300125
     *      ]
     * ];
     *
     * @param  array $parameters
     * @return object
     * @throws Exception
     */
    public function delete($parameters)
    {
        return $this->execute('DeleteCampaign', $parameters);
    }

    /**
     * Get all Campaigns
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *     'campaignId' => 300125
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getAll($parameters = [])
    {
        return $this->execute('GetCampaigns', $parameters);
    }

    /**
     * Get campaign settings before sending
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'campaignType' => array(
     *          'campaignId' => 300125 // int mandatory
     *      )
     *  );
     *
     * @param array $parameters Ass. array with campaignId
     *
     * @return object
     * @throws Exception
     */
    public function getSummary($parameters)
    {

        $request = FlexmailAPI::parametersToArguments($parameters);

        return $this->execute('GetCampaignSummary', $request);
    }

    /**
     * Get all links with link tracking enabled for a campaign
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'campaignType' => array(
     *          'campaignId' => 300125 // int mandatory
     *      )
     *  );
     *
     * @param array $parameters Ass. array with campaignId
     *
     * @return object
     * @throws Exception
     */
    public function getTrackingLinks($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        return $this->execute('GetCampaignTrackingLinks', $request);;
    }

    /**
     * Recall all email addresses that clicked on a specified tracking link
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'trackingLidId' => 24894456 // int mandatory
     *      )
     *  );
     *
     * @param array $parameters Ass. array with trackingLinkId
     *
     * @return object
     * @throws Exception
     */
    public function getTrackingLinkHits($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        return $this->execute('GetTrackingLinkHits', $request);
    }

    /**
     * Send campaign to a single email address
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'testCampaignType' => [
     *          'testCampaignSendToEmailAddress' => 'baz@flexmail.eu'
     *          'testCampaignName' => 'Test Campaigns',
     *          'testCampaignSubject' => 'Test subject',
     *          'testCampaignSenderName' => 'Flexmail',
     *          'testCampaignMessageId' => 207375,
     *          'testCampaignLanguage' => 'en',
     *          'testCampaignReplyEmailAddress' => 'bar@flexmail.eu',
     *          'testCampaignSenderEmailAddress' => 'foo@flexmail.eu',
     *      ]
     *  ];
     *
     * @param  array $parameters
     * @return object
     * @throws Exception
     */
    public function sendTest($parameters)
    {
        return $this->execute('SendTestCampaign', $parameters);
    }

    /**
     * Launch or plan launch of a campaign
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignId' => 12345,
     *      'campaignSendTimestamp' => '2020-09-20T09:00:00',
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function send($parameters)
    {
        return $this->execute('SendCampaign', $parameters);
    }

    /**
     * Request complete or partial history for all email addresses in a campaign
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'campaignId'             => 12345,                 // int mandatory
     *      'timestampFrom'          => '2008-09-20T09:00:00', // string optional
     *      'timestampTill'          => '2013-09-20T09:00:00', // string optional
     *      'campaignHistoryOptions' => array (                // array optional
     *          'campaignSend'              => true,           // boolean optional
     *          'campaignRead'              => true,           // boolean optional
     *          'campaignReadOnline'        => true,           // boolean optional
     *          'campaignLinkClicked'       => true,           // boolean optional
     *          'campaignLinkGroupClicked'  => true,           // boolean optional
     *          'campaignReadInfoPage'      => true,           // boolean optional
     *          'campaignFormVisited'       => true,           // boolean optional
     *          'campaignFormSubmitted'     => true,           // boolean optional
     *          'campaignFormSubmitted'     => true,           // boolean optional
     *          'campaignSurveySubmitted'   => true,           // boolean optional
     *          'campaignForwardSubmitted'  => true,           // boolean optional
     *          'campaignForwardVisited'    => true            // boolean optional
     *      ),
     *      'sort'                   => 0                      // integer optional
     *
     *
     *
     * @param array $parameters Ass. array with campaignId, optional TimeStampFrom,
     *                                  TimeStampTill and sort parameters and optional
     *                                  array with properties for campaignHistoryOptionsType object
     *
     * @return object
     * @throws Exception
     */
    public function history($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        return $this->execute('GetCampaignHistory', $request);
    }

    /**
     * Receive same result indicators as in web reports
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignId' => 12345,
     * ];
     *
     * @param  array $parameters
     * @return object
     * @throws Exception
     */
    public function report($parameters)
    {
        return $this->execute('GetCampaignReport', $parameters);
    }
}