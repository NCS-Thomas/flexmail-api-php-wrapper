<?php

class FlexmailAPI_Account extends FlexmailAPI
{
    /**
     * Recall all bounced email addresses, either by campaign or by mailing list
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignId'     => 12345,                // int optional
     *      'mailingListId'  => 12345,                // int optional
     *      'timestampSince' => '2010-09-20T09:00:00' // string (YYY-MM-DDTHH:ii:ss) optional
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getBounces($parameters = [])
    {
        return $this->execute('GetBounces', $parameters);
    }

    /**
     * Recall all subscribed email addresses by mailing list and/or timestamp
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'mailingListId'  => 12345,                // int optional
     *      'timestampSince' => '2010-09-20T09:00:00' // string (YYY-MM-DDTHH:ii:ss) optional
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getSubscriptions($parameters = [])
    {
        return $this->execute('GetSubscriptions', $parameters);
    }

    /**
     * Recall all unsubscribed email addresses, either by mailing list or by
     * campaign
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignId'     => 12345,                // int optional
     *      'mailingListId'  => 12345,                // int optional
     *      'timestampSince' => '2010-09-20T09:00:00' // string (YYY-MM-DDTHH:ii:ss) optional
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getUnsubscriptions($parameters = [])
    {
        // when requesting by campaign id, timestampSince becomes obsolete
        if (array_key_exists('campaignId', $parameters) && array_key_exists('timestampSince', $parameters)) {
            unset($parameters['timestampSince']);
        }

        return $this->execute('GetUnsubscriptions', $parameters);
    }

    /**
     * Recall all email addresses which profiles have been updated, either by
     * mailing list or by campaign
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'campaignId'     => 12345,                // int optional
     *      'mailingListId'  => 12345,                // int optional
     *      'timestampSince' => '2010-09-20T09:00:00' // string (YYY-MM-DDTHH:ii:ss) optional
     * ];
     *
     * @param  array $parameters
     * @return object
     * @throws Exception
     */
    public function getProfileUpdates($parameters = [])
    {
        // when requesting by campaign id, timestampSince becomes obsolete
        if (array_key_exists('campaignId', $parameters) && array_key_exists('timestampSince', $parameters)) {
            unset($parameters['timestampSince']);
        }

        return $this->execute('GetProfileUpdates', $parameters);
    }

    /**
     * Return # of email, fax and sms credits
     *
     * @return object
     * @throws Exception
     */
    public function getBalance()
    {
        return $this->execute('GetBalance');
    }

}