<?php

class FlexmailAPI_List extends FlexmailAPI
{
    /**
     * Create a new mailing list
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'categoryId' => 38916,
     *      'mailingListName' => 'My List',
     *      'mailingListLanguage' => 'FR',
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function create($parameters)
    {
        return $this->execute('CreateMailingList', $parameters);
    }

    /**
     * Create a new mailing list Segment
     *
     * Parameters example:
     * ------------------
     * $parameters = [
     *      'parentListId' => 38916,
     *      'name' => 'My List',
     *      'language' => 'FR',
     *      'ruleSet' => [
     *          'rules' => [
     *              'function' => 'member of',
     *              'groupsId' => 123456,
     *          ]
     *      ]
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function createSegment($parameters)
    {
        $this->registerPathsToEncodeAsArray(
            [
                '/ruleSet/rules',
                '/ruleSet/rules/ruleSet/rules',
            ]
        );

        return $this->execute('CreateSegment', $parameters);
    }

    /**
     * Update a mailing list
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'mailingListId' => 110138,
     *      'mailingListName' => 'My List',
     *      'mailingListLanguage' => 'FR',
     * ];
     *
     * @param array
     * @return object
     * @throws Exception
     */
    public function update($parameters)
    {
        return $this->execute('UpdateMailingList', $parameters);
    }

    /**
     * Delete a mailing list
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'mailingListId' => 110138,
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function delete($parameters)
    {
        return $this->execute('DeleteMailingList', $parameters);
    }

    /**
     * Delete a mailing list segment
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'mailingListId' => 110138,
     * ];
     *
     * @param $parameters
     * @return object
     * @throws Exception
     */
    public function deleteSegment($parameters)
    {
        return $this->execute('DeleteSegment', $parameters);
    }

    /**
     * Get all mailing lists within a category
     *
     * Parameters example:
     * -------------------
     * $parameters = [
     *      'categoryId' => 154648,
     * ];
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getAll($parameters)
    {
        return $this->execute('GetMailingLists', $parameters);
    }
}