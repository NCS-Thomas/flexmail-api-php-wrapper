<?php

class FlexmailAPI_Form extends FlexmailAPI
{
    /**
     * Return list of all forms in a Flexmail account
     *
     * @return object
     * @throws Exception
     */
    public function getAll()
    {
        return $this->execute('GetForms');
    }
     
    /**
     * Return list of all user submitted date for a given form and optionally 
     * a campaign
     * 
     * Parmeters example:
     * ------------------
     * $parameters = array (
     *     "formId"     =>  12345, //int mandatory
     *     "campaignId" =>  12345,  /int optional
     * );
     * 
     * @param array $parameters Associative array with formId and optional
     *               campaignId
     * 
     * @return formdata
     */
    public function getResults($parameters)
    {
        $request = array();
        
        foreach($parameters as $key => $value):
            $request[$key] = $value;            
        endforeach;        
        
        $response = $this->execute("GetFormResults", $request);

        return $response;
     }
}