<?php

class FlexmailAPI_Contact extends FlexmailAPI
{
    /**
     * Create a new Contact
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'mailingListId'         =>  116911,               // int mandatory
     *      'emailAddressType'      =>  array(                // array mandatory
     *          'emailAddress'      =>  'foobar@flexmail.eu', // string mandatory
     *          'title'             =>  'Mister'              // string optional
     *          'name'              =>  'John',               // string opt
     *          'surname'           =>  'Doe',                // string opt
     *          'address'           => '',                    // string opt
     *          'zipcode'           => '',                    // string opt
     *          'city'              => '',                    // string opt
     *          'country'           => '',                    // string opt
     *          'phone'             => '',                    // string opt
     *          'fax'               => '',                    // string opt
     *          'mobile'            => '',                    // string opt
     *          'website'           => '',                    // string opt
     *          'language'          => '',                    // string opt
     *          'gender'            => '',                    // string opt
     *          'birthday'          => '1968-08-05',          // string (YYYY-MM-DD) opt
     *          'company'           => '',                    // string opt
     *          'function'          => '',                    // string opt
     *          'market'            => '',                    // string opt
     *          'employees'         => 0,                     // int opt
     *          'nace'              => '',                    // string opt
     *          'turnover'          => '',                    // string opt
     *          'vat'               => '',                    // string opt
     *          'keywords'          => '',                    // string opt
     *          'free_field_1'      => '',                    // string opt
     *          'free_field_2'      => '',                    // string opt
     *          'free_field_3'      => '',                    // string opt
     *          'free_field_4'      => '',                    // string opt
     *          'free_field_5'      => '',                    // string opt
     *          'free_field_6'      => '',                    // string opt
     *          'barcode'           => '',                    // string opt
     *          'custom'            => array()                // array opt
     *     )
     * );
     *
     * @param array $parameters Associative array with properties of an
     *                          emailAddressType object and a mailingListId
     *
     * @return object
     * @throws Exception
     */
    public function create($parameters)
    {
        $this->registerPathsToEncodeAsArray([
            '/emailAddressType/custom',
            '/emailAddressType/events',
            '/emailAddressType/groups',
            '/emailAddressType/preferences',
            '/emailAddressType/sources',
        ]);

        $response = $test = $this->execute('CreateEmailAddress', $parameters);

        return $response;
    }

    /**
     * Update a Contact
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'mailingListId'         => 116911,               // int mandatory
     *      'emailAddressType'      => array(                // array mandatory
     *          'flexmailId'        => '1245887'             // int mandory (unless referenceId set)
     *          'referenceId'       => 'my-ref-001'          // string mandatory (unless flexmailId set)
     *          'emailAddress'      => 'foobar@flexmail.eu', // string mandatory
     *          'title'             => 'Mister'              // string optional
     *          'name'              => 'John',               // string opt
     *          'surname'           => 'Doe',                // string opt
     *          'address'           => '',                   // string opt
     *          'zipcode'           => '',                   // string opt
     *          'city'              => '',                   // string opt
     *          'country'           => '',                   // string opt
     *          'phone'             => '',                   // string opt
     *          'fax'               => '',                   // string opt
     *          'mobile'            => '',                   // string opt
     *          'website'           => '',                   // string opt
     *          'language'          => '',                   // string opt
     *          'gender'            => '',                   // string opt
     *          'birthday'          => '1968-08-05',         // string (YYYY-MM-DD) opt
     *          'company'           => '',                   // string opt
     *          'function'          => '',                   // string opt
     *          'market'            => '',                   // string opt
     *          'employees'         => 0,                    // int opt
     *          'nace'              => '',                   // string opt
     *          'turnover'          => '',                   // string opt
     *          'vat'               => '',                   // string opt
     *          'keywords'          => '',                   // string opt
     *          'free_field_1'      => '',                   // string opt
     *          'free_field_2'      => '',                   // string opt
     *          'free_field_3'      => '',                   // string opt
     *          'free_field_4'      => '',                   // string opt
     *          'free_field_5'      => '',                   // string opt
     *          'free_field_6'      => '',                   // string opt
     *          'barcode'           => '',                   // string opt
     *          'custom'            => array()               // array opt
     *     )
     * );
     *
     * @param array $parameters Associative array with properties of an
     *                          emailAddressType object and a mailingListId
     *
     * @return object
     * @throws Exception
     */
    public function update($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute('UpdateEmailAddress', $request);

        return $response;
    }

    /**
     * Delete a Contact
     *
     * Parameters example:
     * ------------------
     * $parameters =   array (
     *      'mailingListId'         => 116911,      // int mandatory
     *      'emailAddressType'      => array(       // array mandatory
     *          'flexmailId'        => '1245887'    // int mandory (unless referenceId set)
     *          'referenceId'       => 'my-ref-001' // string mandatory (unless flexmailId set)
     *     )
     * );
     *
     * @param  array $parameters Associative array with properties of an emailAddressType object
     *                                                                  and mailingListId
     * @return object
     * @throws Exception
     */
    public function delete($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute('DeleteEmailAddress', $request);

        return $response;
    }

    /**
     * Get all Contacts
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'mailingListIds' => array (             // array (of Int (mailingListIds)) optional
     *          102545,
     *          102246
     *      ),
     *      'groupIds' => array (                   // array (of Int (groupIds)) optional
     *          4566,
     *          4456
     *      )
     *      'emailAddressTypeItems' => array (      // array (of EmailAddressType) optional
     *          array (
     *              'referenceId' => 'my-ref-001'
     *          ),
     *          array (
     *              'referenceId' => 'my-ref-002'
     *          )
     *      )
     * );
     *
     * @param array $parameters Associative array with optional
     *                          emailAddressTypeItems array, mailingListIds
     *                          array or groupIds array
     *
     * @return object
     * @throws Exception
     */
    public function getAll($parameters = null)
    {
        if (isset($parameters)):
            $request = array();
            $emailAddressTypeItems = array();
            $mailingListIds = array();
            $groupIds = array();
            foreach ($parameters as $key => $value):
                if ($key == 'emailAddressTypeItems'):
                    foreach ($value as $value2):
                        array_push($emailAddressTypeItems, (object) $value2);
                    endforeach;
                    $request[$key] = $emailAddressTypeItems;

                elseif ($key == 'mailingListIds'):
                    foreach ($value as $value2):
                        array_push($mailingListIds, $value2);
                    endforeach;
                    $request[$key] = $mailingListIds;

                elseif ($key == 'groupIds'):
                    foreach ($value as $value2):
                        array_push($groupIds, $value2);
                    endforeach;
                    $request[$key] = $groupIds;
                endif;
            endforeach;
        endif;


        $response = $this->execute('GetEmailAddresses', $request);

        return $response;
    }

    /**
     * Import multiple Contacts
     *
     * Parameters example:
     * ------------------
     * $parameters = [
     *      'mailingListId' => 117372,
     *      'emailAddressTypeItems' => [
     *          [
     *              'referenceId' => 'my-ref-523c17e372dd',
     *              'emailAddress' => 'foobar@flexmail.eu',
     *              'title' => 'Mister'
     *              'name' => 'John',
     *              'surname' => 'Doe',
     *              'address' => '',
     *              'zipcode' => '',
     *              'city' => '',
     *              'country' => '',
     *              'phone' => '',
     *              'fax' => '',
     *              'mobile' => '',
     *              'website' => '',
     *              'language' => '',
     *              'gender' => '',
     *              'birthday' => '1968-08-05',
     *              'company' => '',
     *              'function' => '',
     *              'market' => '',
     *              'employees' => 0,
     *              'nace' => '',
     *              'turnover' => '',
     *              'vat' => '',
     *              'keywords' => '',
     *              'free_field_1' => '',
     *              'free_field_2' => '',
     *              'free_field_3' => '',
     *              'free_field_4' => '',
     *              'free_field_5' => '',
     *              'free_field_6' => '',
     *              'barcode' => '',
     *              'custom' =>
     *                  [
     *                      'variableName' => ''
     *                      'value' => ''
     *                  ]
     *          ],
     *      ]
     *      'allowDuplicates' => false,
     *      'overwrite' => false,
     *      'synchronise' => false,
     *      'referenceField' => 'email',
     *      'allowBouncedOut' => true,
     * ];
     *
     * @param  array
     * @return object
     * @throws Exception
     */
    public function import($parameters)
    {
        $this->registerPathsToEncodeAsArray([
            '/emailAddressTypeItems',
            '/emailAddressTypeItems/custom',
            '/emailAddressTypeItems/events',
            '/emailAddressTypeItems/groups',
            '/emailAddressTypeItems/preferences',
            '/emailAddressTypeItems/sources',
        ]);

        return $this->execute('ImportEmailAddresses', $parameters);
    }

    /**
     * Request complete or partial history of a Contact
     *
     * Parameters example:
     * ------------------
     * $parameters = array (
     *      'emailAddress'                   => 'foobar@flexmail.eu',  // string mandatory
     *      'timestampFrom'                  => '2008-09-02T14:42:30', // string (YYYY-MM-DDTHH:ii:ss) optional
     *      'timestampTill'                  => '2014-09-02T14:42:30', // string (YYYY-MM-DDTHH:ii:ss) optional
     *      'emailAddressHistoryOptionsType' => array (                // array optional
     *          'created'                     => true,                 // boolean optional
     *          'deleted'                     => true,                 // boolean optional
     *          'profileUpdateVisited'        => true,                 // boolean optional
     *          'profileUpdateSubmitted'      => true,                 // boolean optional
     *          'subscribed'                  => true,                 // boolean optional
     *          'unsubscribed'                => true,                 // boolean optional
     *          'addedToGroup'                => true,                 // boolean optional
     *          'removedFromGroup'            => true,                 // boolean optional
     *          'addedToAccountBlackList'     => true,                 // boolean optional
     *          'removedFromAccountBlackList' => true,                 // boolean optional
     *          'bounced'                     => true,                 // boolean optional
     *          'bouncedOut'                  => true,                 // boolean optional
     *          'campaignSend'                => true,                 // boolean optional
     *          'campaignRead'                => true,                 // boolean optional
     *          'campaignReadOnline'          => true,                 // boolean optional
     *          'campaignLinkClicked'         => true,                 // boolean optional
     *          'campaignLinkGroupClicked'    => true,                 // boolean optional
     *          'campaignReadInfopage'        => true,                 // boolean optional
     *          'campaignFormVisited'         => true,                 // boolean optional
     *          'campaignFormSubmitted'       => true,                 // boolean optional
     *          'campaignSurveyVisi ted'       => true,                 // boolean optional
     *          'campaignSurveySubmitted'     => true,                 // boolean optional
     *          'campaignForwardVisited'      => true,                 // boolean optional
     *          'campaignForwardSubmitted'    => true,                 // boolean optional
     *      )
     *      'sort'                           =>  1 // int optional (0 = asc, 1 = desc)
     * );
     *
     * @param array $parameters Associative arrays with emailAddress and optional
     *                          timestampTill- and from, sort and
     *                          emailAddressHistoryOptionsType parameters
     *
     * @return object
     * @throws Exception
     */
    public function history($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute('GetEmailAddressHistory', $request);

        return $response;
    }

}