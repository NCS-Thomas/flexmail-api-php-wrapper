<?php

class FlexmailAPI_Category extends FlexmailAPI
{    
    /**
     * Create a new Category
     *
     * Parameters example:
     * ------------------ 
     * $parameters = array (
     *      "categoryName" => "My Category" // string mandatory
     * );
     * 
     * @param  array $parameters Associative array with The name of the new category
     * 
     * @return object
     * @throws Exception
     */
    public function create ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("CreateCategory", $request);
        return $response;
    }
     
    /**
     * Update a Category
     * 
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "categoryId"   => 10025         // int mandatory
     *      "categoryName" => "My Category" // string mandatory
     * );
     * 
     * @param  array $parameters Associative array with categoryId and categoryName
     *
     * @return object
     * @throws Exception
     */
    public function update ($parameters)
    {   
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("UpdateCategory", $request);
        return $response;
    }

    /**
     * Delete a Category
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "categoryId"   => 10025 // int mandatory
     * );
     *
     * @param  array $parameters Associative array with categoryId
     *
     * @return object
     * @throws Exception
     */
    public function delete ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("DeleteCategory", $request);
        return $response;
        
    }

    /**
     * Get all Categories
     *
     * @return object
     * @throws Exception
     */
    public function getAll()
    {
        return $this->execute('GetCategories');
    }
}