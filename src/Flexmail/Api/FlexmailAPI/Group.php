<?php

class FlexmailAPI_Group extends FlexmailAPI
{
    /**
     * Create a new Group
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "groupType" => array (             // array mandatory
     *          "groupName" => "My group name" // string mandatory
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of a groupType
     *                          object
     *
     * @return object
     * @throws Exception
     */
    public function create ($parameters)
    {   
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("CreateGroup", $request);
        return $response;
    }

    /**
     * Update a Group
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "groupType" => array (        // array mandatory
     *          "groupId"   => 123456     // int mandatory
     *          "groupName" => "My group" // string mandatory
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of a groupType
     *                          object
     *
     * @return object
     * @throws Exception
     */
    public function update ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);

        $response = $this->execute("UpdateGroup", $request);
        return $response;
        
    }

    /**
     * Delete a Group
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "groupType" => array (    // array mandatory
     *          "groupId"   => 123456 // int mandatory
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of a groupType
     *              object
     *
     * @return object
     * @throws Exception
     */
    public function delete ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("DeleteGroup", $request);
        return $response;
        
    }

    /**
     * Get all Groups
     *
     * @return object
     * @throws Exception
     */
    public function getAll ()
    {
        return $this->execute('GetGroups');
    }

    /**
     * Create a new GroupSubscription
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "groupSubscriptionType" => array (     // array mandatory
     *          "groupId"                 => 27091,       // int mandatory
     *          "emailAddressFlexmailId"  => 31655,       // int mandatory (unless referenceId is set)
     *          "emailAddressReferenceId" => "my-ref-001" // string mandatory (unless flexmailId is set)
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of a groupSubscriptionType object
     *
     * @return object
     * @throws Exception
     */
    public function createSubscription ($parameters)
    {   
        $request = FlexmailAPI::parametersToArguments($parameters);
       
        $response = $this->execute("CreateGroupSubscription", $request);
        return $response;
    }

    /**
     * delete a GroupSubscription
     *
     * Parameters example:
     * -------------------
     * $parameters = array (
     *      "groupSubscriptionType" => array (     // array mandatory
     *          "groupId"                 => 27091,       // int mandatory
     *          "emailAddressFlexmailId"  => 31655,       // int mandatory (unless referenceId is set)
     *          "emailAddressReferenceId" => "my-ref-001" // string mandatory (unless flexmailId is set)
     *      )
     * );
     *
     * @param array $parameters Associative array with properties of an groupSubscriptionType object
     *
     * @return object
     * @throws Exception
     */
    public function deleteSubscription ($parameters)
    {
        $request = FlexmailAPI::parametersToArguments($parameters);
        
        $response = $this->execute("DeleteGroupSubscription", $request);
        return $response;
        
    }
}