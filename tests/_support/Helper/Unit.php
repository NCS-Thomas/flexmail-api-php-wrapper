<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use FlexmailAPI;
use FlexmailAPI_Campaign;
use FlexmailAPI_Category;
use FlexmailAPI_Group;
use FlexmailAPI_List;
use FlexmailAPI_Message;

class Unit extends \Codeception\Module
{
    /**
     * @return int
     * @throws \Exception
     */
    public static function getCampaignId(): int
    {
        /** @var FlexmailAPI_Campaign $api */
        $api = FlexmailAPI::service('Campaign');

        return $api->getAll()->campaignTypeItems[0]->campaignId;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public static function getCategoryId(): int
    {
        /** @var FlexmailAPI_Category $api */
        $api = FlexmailAPI::service('Category');

        return $api->getAll()->categoryTypeItems[0]->categoryId;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public static function getGroupId(): int
    {
        /** @var FlexmailAPI_Group $api */
        $api = FlexmailAPI::service('Group');

        return $api->getAll()->groupTypeItems[0]->groupId;
    }

    /**
     * @param string $type
     * @return int
     * @throws \Exception
     */
    public static function getListId($type = 'list'): int
    {
        /** @var FlexmailAPI_List $service */
        $service = FlexmailAPI::service('List');

        $response = $service->getAll(
            [
                'categoryId' => self::getCategoryId(),
            ]
        );

        $lists = array_values(
            array_filter(
                $response->mailingListTypeItems,
                function($list) use ($type) {
                    return $type === $list->mailingListType;
                }
            )
        );

        return $lists[0]->mailingListId;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public static function getMessageId(): int
    {
        /** @var FlexmailAPI_Message $service */
        $service = FlexmailAPI::service('Message');

        return $service->getAll()->messageTypeItems[0]->messageId;
    }
}
