<?php declare(strict_types=1);

class FlexmailApiTest extends \Codeception\Test\Unit
{
    public function testEnvHasConfiguration(): void
    {
        $this->assertArrayHasKey('FLEXMAIL_USER_ID', $_ENV);
        $this->assertArrayHasKey('FLEXMAIL_USER_TOKEN', $_ENV);
    }

    public function testCanAutoloadFlexmailAPI(): void
    {
        $this->assertTrue(class_exists('FlexmailAPI'));
    }

    public function testCanGetAService(): void
    {
        $service = FlexmailAPI::service('Account');

        $this->assertInstanceOf(FlexmailAPI_Account::class, $service);
    }

    /**
     * @dataProvider canCreateAllService
     *
     * @param $expectedInstanceType
     * @param $serviceName
     */
    public function testCanCreateService(string $expectedInstanceType, string $serviceName): void
    {
        $service = FlexmailAPI::service($serviceName);

        $this->assertInstanceOf($expectedInstanceType, $service);
    }

    /**
     * @return array
     */
    public function canCreateAllService(): array
    {
        return [
            [FlexmailAPI_Account::class, 'Account'],
            [FlexmailAPI_Blacklist::class, 'Blacklist'],
            [FlexmailAPI_Campaign::class, 'Campaign'],
            [FlexmailAPI_Category::class, 'Category'],
            [FlexmailAPI_Contact::class, 'Contact'],
            [FlexmailAPI_File::class, 'File'],
            [FlexmailAPI_Form::class, 'Form'],
            [FlexmailAPI_Group::class, 'Group'],
            [FlexmailAPI_LandingPage::class, 'LandingPage'],
            [FlexmailAPI_List::class, 'List'],
            [FlexmailAPI_Message::class, 'Message'],
            [FlexmailAPI_Template::class, 'Template'],
        ];
    }

    /**
     * @dataProvider canParseParametersToRequestStructureProvider
     *
     * @param string $expected
     * @param array $parameters
     * @param array $keepAsArray
     */
    public function testCanParseParametersToRequestStructure(
        string $expected,
        array $parameters,
        array $keepAsArray
    ): void {
        $api = new class() extends FlexmailAPI
        {
            public function wrapParseArray(array $parameters)
            {
                return $this->parametersToArguments($parameters);
            }

            public function wrapRegisterPaths(array $paths)
            {
                $this->registerPathsToEncodeAsArray($paths);
            }
        };
        $api->wrapRegisterPaths($keepAsArray);

        $this->assertEquals($expected, json_encode($api->wrapParseArray($parameters)));
    }

    public function canParseParametersToRequestStructureProvider(): array
    {
        return [
            [
                '{"Foo":"bar"}',
                ['Foo' => 'bar'],
                [],
            ],
            [
                '{"parentListId":145,"name":"name-foo","language":"NL","ruleSet":{"rules":[{"operator":"","function":"member of","groupId":92614}]}}',
                [
                    'parentListId' => 145,
                    'name' => 'name-foo',
                    'language' => 'NL', // string mandatory
                    'ruleSet' => [
                        'rules' => [
                            [
                                'operator' => '',
                                'function' => 'member of',
                                'groupId' => 92614,
                            ],
                        ],
                    ],
                ],
                ['/ruleSet/rules'],
            ],
        ];
    }
}