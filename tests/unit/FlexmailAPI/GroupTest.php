<?php declare(strict_types=1);

class GroupTest extends \Codeception\Test\Unit
{
    /** @var FlexmailAPI_Group */
    private $service;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('Group');
    }

    /**
     * @throws Exception
     */
    public function testCanGetAll(): void
    {
        $response = $this->service->getAll();

        $this->assertObjectHasAttribute('groupTypeItems', $response);
        $this->assertIsArray($response->groupTypeItems);
    }
}