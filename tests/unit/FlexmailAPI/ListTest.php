<?php declare(strict_types=1);

use Faker\Factory;
use Faker\Generator;
use Helper\Unit;

class ListTest extends \Codeception\Test\Unit
{
    /** @var FlexmailAPI_List */
    private $service;

    /** @var Generator */
    private $faker;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('List');
        $this->faker = Factory::create('en_UK');
    }

    /**
     * @throws Exception
     */
    public function testCanGetAll(): void
    {
        $response = $this->service->getAll(
            [
                'categoryId' => Unit::getCategoryId(),
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
        $this->assertObjectHasAttribute('mailingListTypeItems', $response);
        $this->assertIsArray($response->mailingListTypeItems);
    }

    /**
     * @throws Exception
     */
    public function testCanCreateGroupSegment(): void
    {
        $response = $this->service->createSegment(
            [
                'parentListId' => Unit::getListId('list'),
                'name' => $this->faker->company,
                'language' => 'NL',
                'ruleSet' => [
                    'rules' => [
                        [
                            'operator' => '',
                            'function' => 'member of',
                            'groupId' => Unit::getGroupId(),
                        ],
                    ],
                ],
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
        $this->assertObjectHasAttribute('referenceListId', $response);
        $this->assertIsInt($response->referenceListId);
    }

    /**
     * @throws Exception
     */
    public function testCanUpdateSegment(): void
    {
        $response = $this->service->update(
            [
                'mailingListId' => Unit::getListId('sublist'),
                'mailingListName' => 'Updated: '.$this->faker->company,
                'mailingListLanguage' => 'fr',
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanDeleteSegment(): void
    {
        $response = $this->service->deleteSegment(
            [
                'segmentId' => Unit::getListId('sublist'),
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
    }
}