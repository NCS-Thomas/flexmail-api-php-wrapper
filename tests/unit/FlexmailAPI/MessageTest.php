<?php declare(strict_types=1);

use Faker\Factory;
use Faker\Generator;

class MessageTest extends \Codeception\Test\Unit
{
    const MESSAGE_ID = 1551596;

    /** @var FlexmailAPI_Message */
    private $service;

    /** @var Generator */
    private $faker;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('Message');
        $this->faker = Factory::create('en_UK');
    }

    /**
     * @throws Exception
     */
    public function testCanCreateMessage(): void
    {
        $response = $this->service->create([
            'messageType' => [
                'messageName' => $this->faker->company,
                'messageText' => $this->faker->paragraphs(3, true),
                'messageTextMessage' => $this->faker->randomHtml(),
            ]
        ]);

        $this->assertObjectHasAttribute('messageId', $response);
        $this->assertIsInt($response->messageId);
    }

    /**
     * @throws Exception
     */
    public function testCanUpdateMessage(): void
    {
        $response = $this->service->update([
            'messageType' => [
                'messageId' => self::MESSAGE_ID,
                'messageName' => $this->faker->company,
                'messageText' => $this->faker->paragraphs(3, true),
                'messageTextMessage' => $this->faker->randomHtml(),
            ]
        ]);

        $this->assertObjectHasAttribute('messageId', $response);
        $this->assertIsInt($response->messageId);
    }
}