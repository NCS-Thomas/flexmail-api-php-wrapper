<?php declare(strict_types=1);

class CategoryTest extends \Codeception\Test\Unit
{
    /** @var FlexmailAPI_Category */
    private $service;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('Category');
    }

    /**
     * @throws Exception
     */
    public function testCanGetAll(): void
    {
        $response = $this->service->getAll();

        $this->assertObjectHasAttribute('categoryTypeItems', $response);
        $this->assertIsArray($response->categoryTypeItems);
    }
}