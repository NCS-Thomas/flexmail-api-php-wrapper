<?php declare(strict_types=1);

class AccountTest extends \Codeception\Test\Unit
{
    /** @var FlexmailAPI_Account */
    private $service;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('Account');
    }

    /**
     * @throws Exception
     */
    public function testCanGetBalance(): void
    {
        $response = $this->service->getBalance();

        $this->assertObjectHasAttribute('email', $response);
    }

    /**
     * @throws Exception
     */
    public function testCanGetBounces(): void
    {
        $response = $this->service->getBounces();

        $this->assertObjectHasAttribute('bounceTypeItems', $response);
    }

    /**
     * @throws Exception
     */
    public function testCanGetSubscriptions(): void
    {
        $response = $this->service->getSubscriptions();

        $this->assertObjectHasAttribute('subscriptionTypeItems', $response);
    }

    /**
     * @throws Exception
     */
    public function testCanGetUnsubscriptions(): void
    {
        $response = $this->service->getUnsubscriptions();

        $this->assertObjectHasAttribute('unsubscriptionTypeItems', $response);
    }

    /**
     * @throws Exception
     */
    public function testCanGetProfileUpdates(): void
    {
        $response = $this->service->getProfileUpdates();

        $this->assertObjectHasAttribute('profileUpdateTypeItems', $response);
    }
}