<?php declare(strict_types=1);

use Faker\Factory;
use Faker\Generator;
use Helper\Unit;

class ContactTest extends \Codeception\Test\Unit
{
    const CONTACT_ID_TO_DELETE = 2;
    const CONTACT_ID_TO_UPDATE = 1;
    const GROUP_ID = 92614;
    const MAILING_LIST_ID = 1462602;

    /** @var FlexmailAPI_Contact */
    private $service;

    /** @var Generator */
    private $faker;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('Contact');
        $this->faker = Factory::create('en_UK');
    }

    /**
     * @throws Exception
     */
    public function testCanCreateContact(): void
    {
        $response = $this->service->create(
            [
                'mailingListId' => self::MAILING_LIST_ID,
                'emailAddressType' => [
                    'referenceId' => microtime(),
                    'emailAddress' => $this->faker->email,
                    'language' => 'nl',
                    'groups' => [
                        self::GROUP_ID,
                    ],
                    'custom' => [
                        [
                            'variableName' => 'foo',
                            'value' => 'bar',
                        ],
                    ],
                ],
            ]
        );

        $this->assertObjectHasAttribute('emailAddressId', $response);
        $this->assertIsInt($response->emailAddressId);
    }

    /**
     * @throws Exception
     */
    public function testCanUpdateContactByFlexmailId(): void
    {
        $response = $this->service->update(
            [
                'mailingListId' => self::MAILING_LIST_ID,
                'emailAddressType' => [
                    'flexmailId' => self::CONTACT_ID_TO_UPDATE,
                    'emailAddress' => $this->faker->email,
                ],
            ]
        );

        $this->assertObjectHasAttribute('emailAddressId', $response);
        $this->assertIsInt($response->emailAddressId);
    }

    /**
     * @throws Exception
     */
    public function testCanUpdateContactByReferenceId(): void
    {
        $email = $this->faker->email;
        $this->createContact(Unit::getListId(), Unit::getGroupId(), $email);

        $response = $this->service->update(
            [
                'mailingListId' => self::MAILING_LIST_ID,
                'emailAddressType' => [
                    'referenceId' => $email,
                    'emailAddress' => 'updated-'.$email,
                ],
            ]
        );
        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->errorCode);
        $this->assertObjectHasAttribute('emailAddressId', $response);
        $this->assertIsInt($response->emailAddressId);    }

    /**
     * @throws Exception
     */
    public function testCanDeleteContactByFlexmailId(): void
    {
        $response = $this->service->delete(
            [
                'mailingListId' => self::MAILING_LIST_ID,
                'emailAddressType' => [
                    'flexmailId' => self::CONTACT_ID_TO_DELETE,
                ],
            ]
        );

        $this->assertObjectHasAttribute('errorCode', $response);
        $this->assertIsInt($response->errorCode);
        $this->assertSame(0, $response->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanDeleteContactByReferenceId(): void
    {
        $email = $this->faker->email;
        $this->createContact(Unit::getListId(), Unit::getGroupId(), $email);

        $response = $this->service->delete(
            [
                'mailingListId' => self::MAILING_LIST_ID,
                'emailAddressType' => [
                    'referenceId' => $email,
                ],
            ]
        );

        $this->assertObjectHasAttribute('errorCode', $response);
        $this->assertIsInt($response->errorCode);
        $this->assertSame(0, $response->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanImportContacts(): void
    {
        $response = $this->service->import(
            [
                'mailingListId' => self::MAILING_LIST_ID,
                'emailAddressTypeItems' => [
                    [
                        'referenceId' => microtime(),
                        'emailAddress' => $this->faker->email,
                        'groups' => [
                            self::GROUP_ID,
                        ],
                    ],
                    [
                        'referenceId' => microtime(),
                        'emailAddress' => $this->faker->email,
                        'groups' => [
                            self::GROUP_ID,
                        ],
                    ],
                ],

            ]
        );

        $this->assertObjectHasAttribute('importEmailAddressRespTypeItems', $response);
        $this->assertIsArray($response->importEmailAddressRespTypeItems);
        $this->assertCount(2, $response->importEmailAddressRespTypeItems);
    }

    /**
     * @param int $listId
     * @param int $groupId
     * @param string $email
     * @return int
     * @throws Exception
     */
    private function createContact(int $listId, int $groupId, string $email): int
    {
        return $this->service->create(
            [
                'mailingListId' => $listId,
                'emailAddressType' => [
                    'referenceId' => $email,
                    'emailAddress' => $email,
                    'language' => 'nl',
                    'groups' => [
                        $groupId,
                    ],
                ],
            ]
        )->emailAddressId;
    }
}