<?php declare(strict_types=1);

use Faker\Factory;
use Faker\Generator;
use Helper\Unit;

class CampaignTest extends \Codeception\Test\Unit
{
    /** @var FlexmailAPI_Campaign */
    private $service;

    /** @var Generator */
    private $faker;

    protected function setUp(): void
    {
        $this->service = FlexmailAPI::service('Campaign');
        $this->faker = Factory::create('en_UK');
    }

    /**
     * @throws Exception
     */
    public function testCanCreateCampaign(): void
    {
        $response = $this->service->create(
            [
                'campaignType' => [
                    'campaignName' => $this->faker->company,
                    'campaignSubject' => $this->faker->company,
                    'campaignSenderName' => $this->faker->name,
                    'campaignMessageId' => Unit::getMessageId(),
                    'campaignMailingIds' => [Unit::getListId('sublist')],
                    'campaignReplyEmailAddress' => 'thomas.vangelooven@gmail.com',
                    'campaignSenderEmailAddress' => 'thomas.vangelooven@gmail.com',
                ],
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
        $this->assertObjectHasAttribute('campaignId', $response);
        $this->assertIsInt($response->campaignId);
    }

    /**
     * @throws Exception
     */
    public function testCanGetACampaign(): void
    {
        $campaignId = Unit::getCampaignId();

        $response = $this->service->getAll(
            [
                'campaignId' => $campaignId,
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
        $this->assertObjectHasAttribute('campaignTypeItems', $response);
        $this->assertCount(1, $response->campaignTypeItems);
        $this->assertIsInt($response->campaignTypeItems[0]->campaignId);
        $this->assertEquals($campaignId, $response->campaignTypeItems[0]->campaignId);
    }

    /**
     * @throws Exception
     */
    public function testCanUpdateCampaign(): void
    {
        $response = $this->service->update(
            [
                'campaignType' => [
                    'campaignId' => Unit::getCampaignId(),
                    'campaignName' => 'Update: '.$this->faker->company,
                    'campaignSubject' => 'Update: '.$this->faker->company,
                    'campaignSenderName' => 'Update: '.$this->faker->name,
                    'campaignMessageId' => Unit::getMessageId(),
                    'campaignMailingIds' => [Unit::getListId('sublist')],
                    'campaignReplyEmailAddress' => 'thomas.vangelooven@gmail.com',
                    'campaignSenderEmailAddress' => 'thomas.vangelooven@gmail.com',
                ],
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanDeleteCampaign(): void
    {
        $response = $this->service->update(
            [
                'campaignType' => [
                    'campaignId' => Unit::getCampaignId(),
                ],
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanSendCampaign(): void
    {
        $response = $this->service->send(
            [
                'campaignId' => Unit::getCampaignId(),
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanSendTestCampaign(): void
    {
        $response = $this->service->sendTest(
            [
                'testCampaignType' => [
                    'testCampaignSendToEmailAddress' => 'thomas.vangelooven@gmail.com',
                    'testCampaignName' => $this->faker->company,
                    'testCampaignSubject' => $this->faker->company,
                    'testCampaignSenderName' => $this->faker->name,
                    'testCampaignMessageId' => Unit::getMessageId(),
                    'testCampaignLanguage' => 'en',
                    'testCampaignReplyEmailAddress' => 'thomas.vangelooven@gmail.com',
                    'testCampaignSenderEmailAddress' => 'thomas.vangelooven@gmail.com',
                ],
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
    }

    /**
     * @throws Exception
     */
    public function testCanGetReport(): void
    {
        $response = $this->service->report(
            [
                'campaignId' => Unit::getCampaignId(),
            ]
        );

        $this->assertObjectHasAttribute('header', $response);
        $this->assertObjectHasAttribute('errorCode', $response->header);
        $this->assertSame(0, $response->header->errorCode);
        $this->assertObjectHasAttribute('campaignReportType', $response);
        $this->assertIsObject($response->campaignReportType);
    }
}