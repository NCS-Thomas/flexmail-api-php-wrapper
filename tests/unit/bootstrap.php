<?php

use Symfony\Component\Dotenv\Dotenv;

require __DIR__.'/../../vendor/autoload.php';

$dotEnv = new Dotenv();

if (file_exists(__DIR__.'/.env')) {
    $dotEnv->load(__DIR__.'/.env');
}

if (isset($_ENV['ENVIRONMENT']) && file_exists(__DIR__.'/.env.'.$_ENV['ENVIRONMENT'])) {
    $dotEnv->load(__DIR__.'/.env.'.$_ENV['ENVIRONMENT']);
}